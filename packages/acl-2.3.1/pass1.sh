src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --disable-static \
        --docdir=${PREFIX}/share/doc/${P} \
        --host=${LFS_TGT} \
        --build=$(./build-aux/config.guess)
}
