
src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --disable-static  \
        --sysconfdir=/etc \
        --docdir=${PREFIX}/share/doc/${P} \
        --host=${LFS_TGT} \
        --build=$(./build-aux/config.guess)
}
