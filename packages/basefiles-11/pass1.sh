#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    return
}

src_compile()
{
    return
}

src_install()
{
    mkdir -p ${D}/${LFS}/var/lib/
    echo ${LFS} > ${D}/${LFS}/var/lib/formerbase

    mkdir -p ${D}/${LFS}/etc
    ln -s /proc/self/mounts ${D}/${LFS}/etc/mtab

    cat > ${D}/${LFS}/etc/hosts << EOF
127.0.0.1  localhost $(hostname)
::1        localhost
EOF

    cat > ${D}/${LFS}/etc/passwd << EOF
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/dev/null:/usr/bin/false
daemon:x:6:6:Daemon User:/dev/null:/usr/bin/false
messagebus:x:18:18:D-Bus Message Daemon User:/run/dbus:/usr/bin/false
uuidd:x:80:80:UUID Generation Daemon User:/dev/null:/usr/bin/false
nobody:x:65534:65534:Unprivileged User:/dev/null:/usr/bin/false
$(id -un):x:$(id -u):$(id -g):Current user:/tmp:/bin/bash
EOF

    cat > ${D}/${LFS}/etc/group << EOF
root:x:0:
bin:x:1:daemon
sys:x:2:
kmem:x:3:
tape:x:4:
tty:x:5:
daemon:x:6:
floppy:x:7:
disk:x:8:
lp:x:9:
dialout:x:10:
audio:x:11:
video:x:12:
utmp:x:13:
usb:x:14:
cdrom:x:15:
adm:x:16:
messagebus:x:18:
input:x:24:
mail:x:34:
kvm:x:61:
uuidd:x:80:
wheel:x:97:
users:x:999:
$(id -gn):x:$(id -g):
nogroup:x:65534:
EOF

    echo "tester:x:101:101::/home/tester:/bin/bash" >> ${D}/${LFS}/etc/passwd
    echo "tester:x:101:" >> ${D}/${LFS}/etc/group
    mkdir -p ${D}/${LFS}/home/tester

    mkdir -p ${D}/${LFS}/var/log
    touch \
        ${D}/${LFS}/var/log/btmp \
        ${D}/${LFS}/var/log/lastlog \
        ${D}/${LFS}/var/log/faillog \
        ${D}/${LFS}/var/log/wtmp
#    chgrp utmp ${D}/${LFS}/var/log/lastlog
    chmod 664 ${D}/${LFS}/var/log/lastlog
    chmod 600 ${D}/${LFS}/var/log/btmp
}
