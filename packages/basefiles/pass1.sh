#!/bin/sh
# Copyright © 1999-2017 Gerard Beekmans
# Copyright © 2017-2018 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    return
}

src_compile()
{
    return
}

src_install()
{
    target_dir=${PREFIX#${LFS}}
    inner_path=${D}/${LFS}/$(dirname ${PREFIX})
    inner_name=$(basename ${PREFIX})
    mkdir -p ${inner_path}
    ln -s ${target_dir} ${inner_path}/${inner_name}
    mkdir -p ${D}/${LFS}/var/lib/
    echo ${LFS} > ${D}/${LFS}/var/lib/formerbase

    mkdir -p ${D}/${LFS}/bin
    ln -s ${target_dir}/bin/bash ${D}/${LFS}/bin
    ln -s ${target_dir}/bin/cat ${D}/${LFS}/bin
    ln -s ${target_dir}/bin/echo ${D}/${LFS}/bin
    ln -s ${target_dir}/bin/pwd ${D}/${LFS}/bin
    ln -s ${target_dir}/bin/stty ${D}/${LFS}/bin

    mkdir -p ${D}/${LFS}/usr/bin
    ln -s ${target_dir}/bin/perl ${D}/${LFS}/usr/bin
    ln -s ${target_dir}/bin/env ${D}/${LFS}/usr/bin

    mkdir -p ${D}/${LFS}/usr/lib
    ln -s ${target_dir}/lib/libgcc_s.so ${D}/${LFS}/usr/lib
    ln -s ${target_dir}/lib/libgcc_s.so.1 ${D}/${LFS}/usr/lib
    ln -s ${target_dir}/lib/libstdc++.so ${D}/${LFS}/usr/lib
    ln -s ${target_dir}/lib/libstdc++.so.6 ${D}/${LFS}/usr/lib
    ln -s bash ${D}/${LFS}/bin/sh

    mkdir -p ${D}/${LFS}/etc
    ln -s /proc/self/mounts ${D}/${LFS}/etc/mtab

    cat > ${D}/${LFS}/etc/passwd << EOF
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/dev/null:/bin/false
daemon:x:6:6:Daemon User:/dev/null:/bin/false
messagebus:x:18:18:D-Bus Message Daemon User:/var/run/dbus:/bin/false
nobody:x:99:99:Unprivileged User:/dev/null:/bin/false
$(id -un):x:$(id -u):$(id -g):Current user:/tmp:/bin/bash
EOF

    cat > ${D}/${LFS}/etc/group << EOF
root:x:0:
bin:x:1:daemon
sys:x:2:
kmem:x:3:
tape:x:4:
tty:x:5:
daemon:x:6:
floppy:x:7:
disk:x:8:
lp:x:9:
dialout:x:10:
audio:x:11:
video:x:12:
utmp:x:13:
usb:x:14:
cdrom:x:15:
adm:x:16:
messagebus:x:18:
systemd-journal:x:23:
input:x:24:
mail:x:34:
nogroup:x:99:
users:x:999:
$(id -gn):x:$(id -g):
EOF

    mkdir -p ${D}/${LFS}/var/log
    touch \
        ${D}/${LFS}/var/log/btmp \
        ${D}/${LFS}/var/log/lastlog \
        ${D}/${LFS}/var/log/faillog \
        ${D}/${LFS}/var/log/wtmp
#    chgrp utmp ${D}/${LFS}/var/log/lastlog
    chmod 664 ${D}/${LFS}/var/log/lastlog
    chmod 600 ${D}/${LFS}/var/log/btmp

    case $(uname -m) in
        x86)
            mkdir -p ${D}/${LFS}/lib
            ln -s ld-linux.so.2 ${D}/${LFS}/lib/ld-lsb.so.3
        ;;
        x86_64) 
            mkdir -p ${D}/${LFS}/lib64
            ln -s ../lib/ld-linux-x86-64.so.2 ${D}/${LFS}/lib64
            ln -s ../lib/ld-linux-x86-64.so.2 ${D}/${LFS}/lib64/ld-lsb-x86-64.so.3
        ;;
    esac
    
}
