
src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --without-bash-malloc
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
    ln -s bash ${D}/${PREFIX}/bin/sh
}
