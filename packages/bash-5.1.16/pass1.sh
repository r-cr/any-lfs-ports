#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.

src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --build=$(support/config.guess) \
        --host=$LFS_TGT      \
        --without-bash-malloc
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
    mkdir -p ${D}/bin/
    ln -s bash ${D}/bin/sh
}
