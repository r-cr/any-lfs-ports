
src_config()
{
    return
}

src_compile()
{
    make ${MAKEOPTS} -f Makefile-libbz2_so
    make ${MAKEOPTS} clean
    make ${MAKEOPTS}
}

src_install()
{
    make PREFIX=${D}/${PREFIX} install ${MAKEOPTS}
    cp -f bzip2-shared ${D}${PREFIX}/bin/bzip2
    cp -a libbz2.so* ${D}${PREFIX}/lib
    ln -s libbz2.so.1.0 ${D}${PREFIX}/lib/libbz2.so
}
