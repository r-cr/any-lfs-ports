
src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --enable-install-program=hostname
}

src_compile()
{
    make ${MAKEOPTS}

    if use 'test' ; then
        make RUN_EXPENSIVE_TESTS=yes check
    fi
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
