
src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --enable-install-program=hostname
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}

src_check()
{
    make RUN_EXPENSIVE_TESTS=yes check
}
