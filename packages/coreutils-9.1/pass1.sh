#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.

src_config()
{
    ./configure \
            --prefix=${PREFIX}                \
            --host=${LFS_TGT}                 \
            --build=$(build-aux/config.guess) \
            --enable-install-program=hostname \
            --enable-no-install-program=kill,uptime
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}

    mkdir -p ${D}${PREFIX}/sbin/
    mv ${D}${PREFIX}/bin/chroot ${D}${PREFIX}/sbin/
    mkdir -p ${D}${PREFIX}/share/man/man8
    mv ${D}${PREFIX}/share/man/man1/chroot.1 ${D}${PREFIX}/share/man/man8/chroot.8
    sed -i 's/"1"/"8"/' ${D}${PREFIX}/share/man/man8/chroot.8
}
