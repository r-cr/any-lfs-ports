
src_config()
{
    LIBS="-lacl" \
    ./configure \
        --prefix=${PREFIX} \
        --docdir=${PREFIX}/share/doc/${P} \
        --host=${LFS_TGT}
}
