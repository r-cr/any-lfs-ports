#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    mkdir build
    pushd build
        ../configure \
            --disable-bzlib      \
            --disable-libseccomp \
            --disable-xzlib      \
            --disable-zlib
        make
    popd

    ./configure \
        --prefix=${PREFIX} \
        --host=${LFS_TGT} \
        --build=$(./config.guess)
         
}

src_compile()
{
    make ${MAKEOPTS} FILE_COMPILE=$(pwd)/build/src/file
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
