
src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --localstatedir=/var/lib/locate \
        --host=${LFS_TGT}                 \
        --build=$(build-aux/config.guess)
}
