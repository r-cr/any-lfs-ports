

src_config()
{
    sed -i 's/extras//' Makefile.in

    ./configure \
        --prefix=${PREFIX} \
            --host=${LFS_TGT} \
            --build=$(build-aux/config.guess)
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
