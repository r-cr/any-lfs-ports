#!/bin/sh
# Copyright © 1999-2017 Gerard Beekmans
# Copyright © 2017-2018 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    cd gettext-tools
    EMACS='no' \
    ./configure \
        --prefix=${PREFIX} \
        --disable-shared
}

src_compile()
{
    make -C gnulib-lib ${MAKEOPTS}
    make -C intl pluralx.c ${MAKEOPTS}
    make -C src msgfmt ${MAKEOPTS}
    make -C src msgmerge ${MAKEOPTS}
    make -C src xgettext ${MAKEOPTS}
}

src_install()
{
    mkdir -p ${D}/${PREFIX}/bin
    cp -f src/msgfmt src/msgmerge src/xgettext ${D}/${PREFIX}/bin
}
