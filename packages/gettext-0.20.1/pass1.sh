#!/bin/sh
# Copyright © 1999-2020 Gerard Beekmans
# Copyright © 2017-2020 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --disable-shared
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    mkdir -p ${D}/${PREFIX}/bin
    cp -f gettext-tools/src/msgfmt gettext-tools/src/msgmerge gettext-tools/src/xgettext ${D}/${PREFIX}/bin
}
