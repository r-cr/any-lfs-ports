#!/bin/sh
# Copyright © 1999-2017 Gerard Beekmans
# Copyright © 2017-2018 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    mkdir build
    cd build

    ../configure                         \
      --prefix=${PREFIX}                 \
      --host=${LFS_TGT}                  \
      --build=$(../scripts/config.guess) \
      --enable-kernel=2.6.32             \
      --with-headers=${PREFIX}/include   \
      libc_cv_forced_unwind=yes          \
      libc_cv_c_cleanup=yes
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=$D

    # gcc sickness:
    # modify and install additional specs to add search path for libs and includes
    path=$( ${LFS_TGT}-gcc -print-libgcc-file-name )
    path=$(dirname ${path})
    mkdir -p ${D}/${path}/
    ${LFS_TGT}-gcc -dumpspecs > ${T}/specs
    sed -e "s#crt1.o#${PREFIX}/lib/crt1.o#g" \
        -e "s#crti.o#${PREFIX}/lib/crti.o#g" \
        -e "s#crtn.o#${PREFIX}/lib/crtn.o#g" \
        -e "\#%{posix:-D_POSIX_SOURCE} %{pthread:-D_REENTRANT}#s#\$# -I${PREFIX}/include#" \
         ${T}/specs > ${D}/${path}/specs

    mkdir -p ${D}/${PREFIX}/${LFS_TGT}/include
    for i in $(ls ${D}/${PREFIX}/include/) ; do
        ln -s ../../include/$i ${D}/${PREFIX}/${LFS_TGT}/include/$i
    done
}
