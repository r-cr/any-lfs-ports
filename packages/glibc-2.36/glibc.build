#!/bin/sh
# Copyright © 1999-2020 Gerard Beekmans
# Copyright © 2017-2020 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    mkdir build
    cd build

    CC="gcc -ffile-prefix-map=${PASS1PREFIX}=${PREFIX}" \
    ../configure                                    \
             --prefix=${PREFIX}                     \
             --disable-werror                       \
             --enable-kernel=3.2                    \
             --enable-stack-protector=strong        \
             --with-headers=${PREFIX}/include       \
             libc_cv_slibdir=/lib
}

src_compile()
{
    make ${MAKEOPTS}

    sed '/test-installation/s@$(PERL)@echo not running@' -i ../Makefile
}

src_check()
{
    make check || nonfail
}

src_install()
{
    mkdir -p ${D}/lib ${D}/lib64
    case $(uname -m) in
        i?86)   ln -sf ld-linux.so.2 ${D}/lib/ld-lsb.so.3
        ;;
        x86_64) ln -sf ../lib/ld-linux-x86-64.so.2 ${D}/lib64
                ln -sf ../lib/ld-linux-x86-64.so.2 ${D}/lib64/ld-lsb-x86-64.so.3
        ;;
    esac

    make install ${MAKEOPTS} DESTDIR=$D
    cp ../nscd/nscd.conf ${D}/etc/nscd.conf
    mkdir -p ${D}/var/cache/nscd/

    mkdir -p ${D}/usr/lib/locale/
    localedef -i POSIX -f UTF-8 ${D}/usr/lib/locale/C.UTF-8 2> /dev/null || true
    localedef -i cs_CZ -f UTF-8 ${D}/usr/lib/locale/cs_CZ.UTF-8
    localedef -i de_DE -f ISO-8859-1 ${D}/usr/lib/locale/de_DE
    localedef -i de_DE@euro -f ISO-8859-15 ${D}/usr/lib/locale/de_DE@euro
    localedef -i de_DE -f UTF-8 ${D}/usr/lib/locale/de_DE.UTF-8
    localedef -i en_GB -f UTF-8 ${D}/usr/lib/locale/en_GB.UTF-8
    localedef -i en_HK -f ISO-8859-1 ${D}/usr/lib/locale/en_HK
    localedef -i en_PH -f ISO-8859-1 ${D}/usr/lib/locale/en_PH
    localedef -i en_US -f ISO-8859-1 ${D}/usr/lib/locale/en_US
    localedef -i en_US -f UTF-8 ${D}/usr/lib/locale/en_US.UTF-8
    localedef -i es_MX -f ISO-8859-1 ${D}/usr/lib/locale/es_MX
    localedef -i fa_IR -f UTF-8 ${D}/usr/lib/locale/fa_IR
    localedef -i fr_FR -f ISO-8859-1 ${D}/usr/lib/locale/fr_FR
    localedef -i fr_FR@euro -f ISO-8859-15 ${D}/usr/lib/locale/fr_FR@euro
    localedef -i fr_FR -f UTF-8 ${D}/usr/lib/locale/fr_FR.UTF-8
    localedef -i it_IT -f ISO-8859-1 ${D}/usr/lib/locale/it_IT
    localedef -i it_IT -f UTF-8 ${D}/usr/lib/locale/it_IT.UTF-8
    localedef -i ja_JP -f EUC-JP ${D}/usr/lib/locale/ja_JP
    localedef -i ru_RU -f KOI8-R ${D}/usr/lib/locale/ru_RU.KOI8-R
    localedef -i ru_RU -f UTF-8 ${D}/usr/lib/locale/ru_RU.UTF-8
    localedef -i tr_TR -f UTF-8 ${D}/usr/lib/locale/tr_TR.UTF-8
    localedef -i zh_CN -f GB18030 ${D}/usr/lib/locale/zh_CN.GB18030
    
    cat > ${D}/etc/nsswitch.conf << "EOF"
# Begin /etc/nsswitch.conf

passwd: files
group: files
shadow: files

hosts: files dns
networks: files

protocols: files
services: files
ethers: files
rpc: files

# End /etc/nsswitch.conf
EOF

    mkdir -p ${D}/etc/ld.so.conf.d/
    cat > /etc/ld.so.conf << "EOF"
# Begin /etc/ld.so.conf
/usr/local/lib
/opt/lib

# Add an include directory
include /etc/ld.so.conf.d/*.conf

EOF

}
