#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.

STRIP=$(which true)

src_config()
{
    mkdir build
    cd build

    echo "rootsbindir=/usr/sbin" > configparms
    ../configure                         \
      --prefix=${PREFIX}                 \
      --host=${LFS_TGT}                  \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2                \
      --with-headers=${LFS}${PREFIX}/include   \
      libc_cv_slibdir=${PREFIX}/lib

}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    case $(uname -m) in
        i?86)
            mkdir -p ${D}/lib
            ln -sf ld-linux.so.2 ${D}/lib/ld-lsb.so.3
        ;;
        x86_64) 
            mkdir -p ${D}/lib64
            ln -sf ../lib/ld-linux-x86-64.so.2 ${D}/lib64
            ln -sf ../lib/ld-linux-x86-64.so.2 ${D}/lib64/ld-lsb-x86-64.so.3
        ;;
    esac

    make install ${MAKEOPTS} DESTDIR=$D

    sed '/RTLDLIST=/s@/usr@@g' -i ${D}${PREFIX}/bin/ldd
    # this belongs to gcc, installed to filesystem before
    ${LFS}${PASS1PREFIX}/libexec/gcc/${LFS_TGT}/12.2.0/install-tools/mkheaders
}
