
src_install()
{
    make RAISE_SETFCAP=no lib=lib prefix=${PREFIX} DESTDIR=${D} install
    chmod -v 755 ${D}/${PREFIX}/lib/libcap.so
}
