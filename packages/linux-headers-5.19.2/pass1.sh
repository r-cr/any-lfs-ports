
src_install()
{
    make headers

    find usr/include -type f ! -name '*.h' -delete
    mkdir -p ${D}/${PREFIX}
    cp -r usr/include ${D}/${PREFIX}/
}
