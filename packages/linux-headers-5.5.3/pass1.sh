
src_install()
{
    make headers

    mkdir -p ${D}/${PREFIX}/include
    cp -r usr/include/* ${D}/${PREFIX}/include
}
