#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --host=${LFS_TGT} \
        --build=$(build-aux/config.guess)
}
