#!/bin/sh
# Copyright © 1999-2020 Gerard Beekmans
# Copyright © 2017-2020 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    sed -i s/mawk// configure

    ./configure \
        --prefix=${PREFIX} \
            --with-shared   \
            --without-debug \
            --without-ada   \
            --enable-widec  \
            --enable-overwrite
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
    ln -s libncursesw.so ${D}${PREFIX}/lib/libncurses.so
}
