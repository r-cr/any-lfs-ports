#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    sed -i s/mawk// configure

    mkdir build
    pushd build
        ../configure
        make -C include
        make -C progs tic
    popd

    ./configure \
            --prefix=${PREFIX}           \
            --host=${LFS_TGT}            \
            --build=$(./config.guess)    \
            --mandir=${PREFIX}/share/man \
            --with-manpage-format=normal \
            --with-shared                \
            --without-normal             \
            --with-cxx-shared            \
            --without-debug              \
            --without-ada                \
            --disable-stripping          \
            --enable-widec
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    make DESTDIR=${D} TIC_PATH=$(pwd)/build/progs/tic install ${MAKEOPTS}
    echo "INPUT(-lncursesw)" > ${D}${PREFIX}/lib/libncurses.so
}
