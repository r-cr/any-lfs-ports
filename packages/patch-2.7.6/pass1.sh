src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        $(ause_enable 'crosstch' "--host=${LFS_TGT}") \
        $(ause_enable 'crosstch' "--build=$(build-aux/config.guess)")
}
