#!/bin/sh
# Copyright © 1999-2017 Gerard Beekmans
# Copyright © 2017-2018 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    sh Configure \
        -des \
        -Dprefix=${PREFIX} \
        -Dlibs=-lm
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    mkdir -p ${D}/${PREFIX}/bin
    cp -f perl cpan/podlators/scripts/pod2man ${D}/${PREFIX}/bin/
    mkdir -p ${D}/${PREFIX}/lib/perl5/5.24.1
    cp -fR lib/* ${D}/${PREFIX}/lib/perl5/5.24.1/
}
