#!/bin/sh
# Copyright © 1999-2020 Gerard Beekmans
# Copyright © 2017-2020 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    sh Configure \
        -des \
        -Dprefix=${PREFIX} \
        -Dlibs=-lm \
        -Uloclibpth \
        -Ulocincpth
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    mkdir -p ${D}/${PREFIX}/bin
    cp -f perl cpan/podlators/scripts/pod2man ${D}/${PREFIX}/bin/
    mkdir -p ${D}/${PREFIX}/lib/perl5/${PV}/
    cp -fR lib/* ${D}/${PREFIX}/lib/perl5/${PV}/
}
