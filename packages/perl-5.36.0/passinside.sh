#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    sh Configure \
        -des               \
        -Dprefix=${PREFIX} \
        -Dvendorprefix=${PREFIX}                         \
        -Dprivlib=${PREFIX}/lib/perl5/${PV}/core_perl     \
        -Darchlib=${PREFIX}/lib/perl5/${PV}/core_perl     \
        -Dsitelib=${PREFIX}/lib/perl5/${PV}/site_perl     \
        -Dsitearch=${PREFIX}/lib/perl5/${PV}/site_perl    \
        -Dvendorlib=${PREFIX}/lib/perl5/${PV}/vendor_perl \
        -Dvendorarch=${PREFIX}/lib/perl5/${PV}/vendor_perl \
        -Dman1dir=${PREFIX}/share/man/man1 \
        -Dman3dir=${PREFIX}/share/man/man3
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
