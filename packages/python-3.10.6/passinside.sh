
src_config()
{
    ./configure \
        --prefix=${PREFIX} \
            --enable-shared \
            --without-ensurepip
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
