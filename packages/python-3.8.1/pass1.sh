
src_config()
{
    sed -i '/def add_multiarch_paths/a \        return' setup.py

    ./configure \
        --prefix=${PREFIX} \
        --without-ensurepip
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
