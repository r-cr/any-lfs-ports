#!/bin/sh
# Copyright © 1999-2017 Gerard Beekmans
# Copyright © 2017-2018 Random Crew
# Distributed under the terms of MIT License.


src_config()
{
    return
}

src_compile()
{
    return
}

src_install()
{
    mkdir -p \
        ${D}/${LFS}/bin/ \
        ${D}/${LFS}/boot/ \
        ${D}/${LFS}/etc/opt/ \
        ${D}/${LFS}/etc/sysconfig/ \
        ${D}/${LFS}/home/ \
        ${D}/${LFS}/lib/firmware/ \
        ${D}/${LFS}/mnt/ \
        ${D}/${LFS}/opt/ \
        ${D}/${LFS}/media/floppy/ \
        ${D}/${LFS}/media/cdrom/ \
        ${D}/${LFS}/sbin/ \
        ${D}/${LFS}/srv/ \
        ${D}/${LFS}/var/
    install -d 0750 ${D}/${LFS}/root/
    install -d -m 1777 ${D}/${LFS}/tmp/ ${D}/${LFS}/var/tmp/
    mkdir -p \
        ${D}/${LFS}/usr/local/bin/ \
        ${D}/${LFS}/usr/local/include/ \
        ${D}/${LFS}/usr/local/lib/ \
        ${D}/${LFS}/usr/local/sbin/ \
        ${D}/${LFS}/usr/local/src/ \
        ${D}/${LFS}/usr/local/share/color/ \
        ${D}/${LFS}/usr/local/share/dict/ \
        ${D}/${LFS}/usr/local/share/doc/ \
        ${D}/${LFS}/usr/local/share/info/ \
        ${D}/${LFS}/usr/local/share/locale/ \
        ${D}/${LFS}/usr/local/share/man/man1/ \
        ${D}/${LFS}/usr/local/share/man/man2/ \
        ${D}/${LFS}/usr/local/share/man/man3/ \
        ${D}/${LFS}/usr/local/share/man/man4/ \
        ${D}/${LFS}/usr/local/share/man/man5/ \
        ${D}/${LFS}/usr/local/share/man/man6/ \
        ${D}/${LFS}/usr/local/share/man/man7/ \
        ${D}/${LFS}/usr/local/share/man/man8/ \
        ${D}/${LFS}/usr/local/share/misc/ \
        ${D}/${LFS}/usr/local/share/terminfo/ \
        ${D}/${LFS}/usr/local/share/zoneinfo/ \
        ${D}/${LFS}/usr/bin/ \
        ${D}/${LFS}/usr/include/ \
        ${D}/${LFS}/usr/lib/ \
        ${D}/${LFS}/usr/sbin/ \
        ${D}/${LFS}/usr/src/ \
        ${D}/${LFS}/usr/share/color/ \
        ${D}/${LFS}/usr/share/dict/ \
        ${D}/${LFS}/usr/share/doc/ \
        ${D}/${LFS}/usr/share/info/ \
        ${D}/${LFS}/usr/share/locale/ \
        ${D}/${LFS}/usr/share/man/man1/ \
        ${D}/${LFS}/usr/share/man/man2/ \
        ${D}/${LFS}/usr/share/man/man3/ \
        ${D}/${LFS}/usr/share/man/man4/ \
        ${D}/${LFS}/usr/share/man/man5/ \
        ${D}/${LFS}/usr/share/man/man6/ \
        ${D}/${LFS}/usr/share/man/man7/ \
        ${D}/${LFS}/usr/share/man/man8/ \
        ${D}/${LFS}/usr/share/misc/ \
        ${D}/${LFS}/usr/share/terminfo/ \
        ${D}/${LFS}/usr/share/zoneinfo/ \
        ${D}/${LFS}/usr/libexec/

    case $(uname -m) in
        *64)
            mkdir -p ${D}/${LFS}/lib64
            ;;
    esac

    mkdir -p \
        ${D}/${LFS}/var/log/ \
        ${D}/${LFS}/var/mail/ \
        ${D}/${LFS}/var/spool/ \
        ${D}/${LFS}/var/opt/ \
        ${D}/${LFS}/var/cache/ \
        ${D}/${LFS}/var/lib/color/ \
        ${D}/${LFS}/var/lib/misc/ \
        ${D}/${LFS}/var/lib/locate/ \
        ${D}/${LFS}/var/local/
}
