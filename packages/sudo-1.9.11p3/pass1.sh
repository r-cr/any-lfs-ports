# this cancels the effect of empty lfs_postinstall in main build
loadlib lfs

src_config()
{
    ./configure \
        --prefix=${PREFIX} \
        --libexecdir=${PREFIX}/lib      \
        --with-secure-path         \
        --with-all-insults         \
        --with-env-editor          \
        --docdir=${PREFIX}/share/doc/${P} \
        --with-passprompt="[sudo] password for %p: " \
        --host=${LFS_TGT} \
        --build=$(./scripts/config.guess)
}
