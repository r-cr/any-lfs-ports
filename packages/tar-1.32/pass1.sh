
src_config()
{
    ./configure \
        --prefix=${PREFIX}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
