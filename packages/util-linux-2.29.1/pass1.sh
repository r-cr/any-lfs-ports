#!/bin/sh
# Copyright © 1999-2017 Gerard Beekmans
# Copyright © 2017-2018 Random Crew
# Distributed under the terms of MIT License.

loadlib lfs

src_config()
{
    ./configure \
        --disable-silent-rules \
        --prefix=${PREFIX} \
        --without-python               \
        --disable-makeinstall-chown    \
        --without-systemdsystemunitdir \
        PKG_CONFIG="" \
        LDFLAGS="-ldl"
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
