#!/bin/sh
# Copyright © 1999-2022 Gerard Beekmans
# Copyright © 2017-2022 Random Crew
# Distributed under the terms of MIT License.

# this undoes empty lfs_postinstall from the main build
loadlib lfs

src_config()
{
    ./configure \
        ADJTIME_PATH=/var/lib/hwclock/adjtime    \
        --libdir=${PREFIX}/lib    \
        --docdir=${PREFIX}/share/doc/${P} \
        --disable-chfn-chsh  \
        --disable-login      \
        --disable-nologin    \
        --disable-su         \
        --disable-setpriv    \
        --disable-runuser    \
        --disable-pylibmount \
        --disable-static     \
        --without-python     \
        runstatedir=/run     \
        --host=${LFS_TGT}    \
        --build=$(config/config.guess) \
        --without-tinfo \
        --disable-makeinstall-chown

    # NCURSES_LIBS in cmd for config does not work
    sed -i -e '/NCURSES_LIBS/s/ -ltinfo//' ./config.status
}

src_compile()
{
    make ${MAKEOPTS}
}

src_install()
{
    mkdir -p ${D}/var/lib/hwclock

    make install ${MAKEOPTS} DESTDIR=${D}
}
