
src_config()
{
    ./configure \
        --prefix=${PREFIX}                \
        --host=${LFS_TGT}                 \
        --build=$(build-aux/config.guess) \
        --disable-static                  \
        --docdir=${PREFIX}/share/doc/${P}
}

src_install()
{
    make install ${MAKEOPTS} DESTDIR=${D}
}
